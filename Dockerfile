FROM node:alpine

RUN apk add --no-cache --virtual .gyp python make g++ libtool autoconf automake libsodium alpine-sdk
RUN npm install cabal
RUN apk del .gyp
